//+build wireinject

package wire

import (
	"github.com/google/wire"
	"test/internal/cache"
	"test/internal/cache/mem"
	"test/internal/command"
	"test/internal/conf"
	"test/internal/server"
	"test/internal/storage"
)

func InitServer() *server.Server {

	wire.Build(
		conf.ProvideDefaultConfig,
		command.ProvideCommandHandler,
		storage.NewFileStorage,
		wire.Bind(new(storage.IStorage), new(*storage.FileStorage)),
		wire.Bind(new(server.IHandler), new(*command.CommandHandler)),
		mem.ProvideMemCache, wire.Bind(new(cache.ICache), new(*mem.MemCache)),
		server.ProvideServer,
	)
	return &server.Server{}
}
