package server

import (
	"net"
	"time"
)

type Conn struct {
	net.Conn
	IdleTimeout time.Duration
}

func (c *Conn) Read(b []byte) (size int, err error) {
	size, err = c.Conn.Read(b)
	c.updateDeadline()
	return
}

func (c *Conn) updateDeadline() {
	idleDeadline := time.Now().Add(c.IdleTimeout)
	c.Conn.SetDeadline(idleDeadline)
}

func (c *Conn) Write(b []byte) (size int, err error) {
	size, err = c.Conn.Write(b)
	c.updateDeadline()
	return
}
