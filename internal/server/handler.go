package server

type IHandler interface {
	Execute(string) (string, error)
}
