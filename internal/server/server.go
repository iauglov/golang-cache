package server

import (
	"bufio"
	"fmt"
	"net"
	"strings"
	"test/internal/conf"
	"time"
)

import "log"

type Server struct {
	idleTimeout    time.Duration
	listener       net.Listener
	commandHandler IHandler
}

const (
	errorFormat = "Error: %v(%v)"
)

func ProvideServer(config *conf.Config, handler IHandler) *Server {
	return &Server{
		idleTimeout:    config.ClientIdleTimeout,
		commandHandler: handler,
	}
}

func (s *Server) StartServer() {
	ln, err := net.Listen("tcp", ":8080")
	if err != nil {
		log.Fatal(err.Error())
	}
	s.listener = ln
	defer ln.Close()

	for {
		conn, err := ln.Accept()
		if err != nil {
			log.Print(err)
			continue
		}

		log.Printf("Client connected: %s", conn.RemoteAddr())
		go s.handle(&Conn{
			Conn:        conn,
			IdleTimeout: s.idleTimeout,
		})
	}
}

func (s *Server) handle(conn *Conn) {
	defer func() {
		log.Printf("Close %s %v", conn.RemoteAddr(), conn.Close())
	}()
	writer := bufio.NewWriter(conn)
	reader := bufio.NewReader(conn)

	for {
		rawCmd, err := reader.ReadString('\n')

		if err != nil {
			log.Printf(errorFormat, err, conn.RemoteAddr())
			return
		}

		cmd := strings.ReplaceAll(rawCmd, "\n", "")
		cmd = strings.ReplaceAll(cmd, "\r\n", "")
		res, err := s.commandHandler.Execute(cmd)

		if err != nil {
			res = err.Error()
		}

		_, err = writer.WriteString(fmt.Sprintf("%s\n", res))
		if err != nil {
			log.Printf("Error: %v(%v)", err, conn.RemoteAddr())
			return
		}

		err = writer.Flush()
		if err != nil {
			log.Printf("Error: %v(%v)", err, conn.RemoteAddr())
			return
		}
	}
}

