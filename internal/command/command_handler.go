package command

import (
	"errors"
	"fmt"
	"strconv"
	"strings"
	"test/internal/cache"
)

const (
	errArgs                = "ERR wrong number of arguments for '%s' command"
	errUnknownCommand      = "ERR unknown command '%s'"
	errInvalidArgumentType = "ERR invalid argument type '%s'"
)

type Command func(...string) (string, error)

type CommandHandler struct {
	cache    cache.ICache
	commands map[string]Command
}

func ProvideCommandHandler(cache cache.ICache) *CommandHandler {
	c := &CommandHandler{
		cache:    cache,
		commands: map[string]Command{},
	}
	c.commands["put"] = c.put
	c.commands["read"] = c.read
	c.commands["delete"] = c.delete
	return c
}

func (c *CommandHandler) Execute(req string) (res string, err error) {
	command, args := c.parseCommand(req)
	if command != nil {
		s, err := command(args...)
		return s, err
	}
	err = errors.New(fmt.Sprintf(errUnknownCommand, req))
	return
}

func (c *CommandHandler) parseCommand(command string) (Command, []string) {
	split := strings.Split(command, " ")
	if command, ok := c.commands[strings.ToLower(split[0])]; ok {
		return command, split[1:]
	}
	return nil, nil
}

func (c *CommandHandler) put(args ...string) (res string, err error) {
	if err = c.checkCommandArgs(len(args), 2, 3, "put"); err != nil {
		return
	}
	var ttl int64 = 0
	if len(args) == 3 {
		ttl, err = strconv.ParseInt(args[2], 10, 64)
		if err != nil {
			return
		}
	}
	c.cache.Put(args[0], args[1], ttl)
	res = "OK"
	return
}

func (c *CommandHandler) delete(args ...string) (res string, err error) {
	if err = c.checkCommandArgs(len(args), 1, 1, "del"); err != nil {
		return
	}
	c.cache.Delete(args[0])
	res = "OK"
	return
}

func (c *CommandHandler) read(args ...string) (res string, err error) {
	if err = c.checkCommandArgs(len(args), 1, 1, "read"); err != nil {
		return
	}
	return c.cache.Read(args[0])
}

func (c *CommandHandler) checkCommandArgs(argsCount, minArgsSize, maxArgSize int, commandName string) error {
	if argsCount < minArgsSize || argsCount > maxArgSize {
		return errors.New(fmt.Sprintf(errArgs, commandName))
	}
	return nil
}
