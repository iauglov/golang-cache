package storage

import (
	"bufio"
	"log"
	"os"
	"os/signal"
	"syscall"
	"test/internal/conf"
)

type FileStorage struct {
	file     *os.File
	updateCh chan StoreItem
}

func NewFileStorage(cfg *conf.Config) *FileStorage {
	file, err := os.OpenFile(cfg.DataFilePath, os.O_RDWR|os.O_CREATE, 0666)
	if err != nil {
		log.Fatalf("Error open data file: %s", cfg.DataFilePath)
	}

	storage := &FileStorage{
		file:     file,
		updateCh: make(chan StoreItem),
	}

	go storage.writeFromPipe()

	return storage
}

func (f *FileStorage) DataPipe() chan StoreItem {
	return f.updateCh
}

func (f *FileStorage) writeFromPipe() {
	defer f.file.Close()
	sigChan := make(chan os.Signal, 1)
	signal.Notify(sigChan,
		syscall.SIGHUP,
		syscall.SIGINT,
		syscall.SIGTERM,
		syscall.SIGQUIT)

LOOP:
	for {
		select {
		case store := <-f.updateCh:
			bytes := store.String()
			f.file.WriteString(bytes)
			f.file.Sync()
		case <-sigChan:
			break LOOP
		}
	}
}

func (f *FileStorage) Load() (res []*StoreItem) {
	res = make([]*StoreItem, 0)
	scanner := bufio.NewScanner(f.file)
	for scanner.Scan() {
		res = append(res, fromString(scanner.Text()))
	}

	return
}
