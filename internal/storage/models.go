package storage

import (
	"log"
	"strconv"
	"strings"
)

type StoreCommandType string

const (
	StoreCommandPut    = StoreCommandType("PUT")
	StoreCommandDelete = StoreCommandType("DELETE")
)

type StoreModel []*StoreItem

type StoreItem struct {
	Key     string
	Value   string
	Time    int64
	Type    StoreCommandType
	DieTime int64
}

const (
	separator = '|'
)

func (i StoreItem) String() string {
	buffer := strings.Builder{}
	buffer.WriteString(i.Key)
	buffer.WriteByte(separator)

	buffer.WriteString(i.Value)
	buffer.WriteByte(separator)

	buffer.WriteString(string(i.Type))
	buffer.WriteByte(separator)

	buffer.WriteString(strconv.FormatInt(i.Time, 10))
	buffer.WriteByte(separator)

	buffer.WriteString(strconv.FormatInt(i.DieTime, 10))
	buffer.WriteByte('\n')
	return buffer.String()
}

func fromString(data string) *StoreItem {
	split := strings.Split(data, string(separator))
	cmdTime, err := strconv.ParseInt(split[3], 10, 64)
	if err != nil {
		log.Printf("Error restore record:\n\t %s", data)
	}
	dieTimeUnix, err := strconv.ParseInt(split[4], 10, 64)
	if err != nil {
		log.Printf("Error restore record dieTime:\n\t %s", data)
	}

	return &StoreItem{
		Key:     split[0],
		Value:   split[1],
		Time:    cmdTime,
		Type:    StoreCommandType(split[2]),
		DieTime: dieTimeUnix,
	}
}
