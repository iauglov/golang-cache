package storage

type IStorage interface {
	Load() []*StoreItem
	DataPipe() chan StoreItem
}
