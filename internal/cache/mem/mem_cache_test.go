package mem

import (
	"test/internal/storage"
	"testing"
	"time"
)

type FakeStorage struct {
}

func (f FakeStorage) Load() []*storage.StoreItem {
	return make([]*storage.StoreItem, 0)
}

func (f FakeStorage) DataPipe() chan storage.StoreItem {
	return make(chan storage.StoreItem, 100)
}

func TestMemCache_Read(t *testing.T) {
	cache := ProvideMemCache(FakeStorage{})
	_, err := cache.Read("asd")
	if err == nil {
		t.Fail()
	}
	cache.Put("asd", "asd", 0)
	read, err := cache.Read("asd")
	if err != nil || read != "asd" {
		t.Fail()
	}
}

func TestMemCache_Delete(t *testing.T) {
	cache := ProvideMemCache(FakeStorage{})
	cache.Put("asd", "asd", 0)
	read, err := cache.Read("asd")
	if err != nil || read != "asd" {
		t.Fail()
	}
	cache.Delete("asd")
	read, err = cache.Read("asd")
	if err == nil {
		t.Fail()
	}
}

func TestMemCache_AutoClean(t *testing.T) {
	cache := ProvideMemCache(FakeStorage{})
	cache.Put("asd", "asd", 1)
	read, err := cache.Read("asd")
	if err != nil || read != "asd" {
		t.Fail()
	}
	time.Sleep(2 * time.Second)
	_, err = cache.Read("asd")
	if err == nil {
		t.Fail()
	}

}
