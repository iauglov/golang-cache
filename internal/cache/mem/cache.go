package mem

import (
	"errors"
	"sort"
	"sync"
	"test/internal/storage"
	"time"
)

type MemCache struct {
	cache     sync.Map
	storePipe chan<- storage.StoreItem
}

func ProvideMemCache(storage storage.IStorage) *MemCache {
	cache := MemCache{
		storePipe: storage.DataPipe(),
	}
	cache.restore(storage.Load())
	go cache.cleanUpAndSave()
	return &cache
}

func (c *MemCache) restore(storeData []*storage.StoreItem) {
	sort.Slice(storeData, func(i, j int) bool {
		return storeData[i].Time < storeData[j].Time
	})

	for _, storeItem := range storeData {
		switch storeItem.Type {
		case storage.StoreCommandPut:
			var dieTime *time.Time = nil
			if storeItem.DieTime != 0 {
				tmp := time.Unix(storeItem.DieTime, 0)
				dieTime = &tmp
			}
			c.cache.Store(storeItem.Key, item{
				data:    storeItem.Value,
				dieTime: dieTime,
			})
		case storage.StoreCommandDelete:
			c.cache.Delete(storeItem.Key)
		}
	}
}

func (c *MemCache) Put(key, value string, ttl int64) {
	var dieTime *time.Time = nil
	if ttl != 0 {
		add := time.Now().Add(time.Duration(ttl) * time.Second)
		dieTime = &add
	}
	c.cache.Store(key, item{
		data:    value,
		dieTime: dieTime,
	})
	go func(key, value string, dieTime *time.Time) {
		var dieTimeUnix int64 = 0
		if dieTime != nil {
			dieTimeUnix = dieTime.Unix()
		}
		c.storePipe <- storage.StoreItem{
			Key:     key,
			Value:   value,
			Time:    time.Now().UnixNano(),
			Type:    storage.StoreCommandPut,
			DieTime: dieTimeUnix,
		}
	}(key, value, dieTime)

}

func (c *MemCache) Delete(key string) {
	c.cache.Delete(key)
	go func(key string) {
		c.storePipe <- storage.StoreItem{
			Key:  key,
			Time: time.Now().UnixNano(),
			Type: storage.StoreCommandDelete,
		}
	}(key)
}

func (c *MemCache) Read(key string) (res string, err error) {
	load, ok := c.cache.Load(key)
	if !ok {
		err = errors.New("(nil)")
		return
	}
	res = load.(item).data
	return
}

func (c *MemCache) cleanUpAndSave() {
	tick := time.Tick(time.Second)
	for range tick {
		c.cache.Range(func(key, value interface{}) bool {
			data := value.(item)
			if data.dieTime != nil && data.dieTime.Before(time.Now()) {
				c.Delete(key.(string))
			}
			return true
		})
	}

}
