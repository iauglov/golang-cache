package cache

type ICache interface {
	Put(key, value string, ttl int64)
	Read(key string) (string, error)
	Delete(key string)
}
