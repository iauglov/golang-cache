package conf

import "time"

type Config struct {
	ClientIdleTimeout time.Duration
	DataFilePath      string
}

func ProvideDefaultConfig() *Config {
	return &Config{
		ClientIdleTimeout: time.Minute * 30,
		DataFilePath:      "./test.dat",
	}
}
